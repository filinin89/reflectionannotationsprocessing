import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException {

        Class instance = Class.forName("Entity");
        Method[] methodsD = instance.getDeclaredMethods(); // объявленные в классе методы

        for(Method method : methodsD){

            // имя метода
            System.out.print(method.getName());

            // параметры метода
            System.out.print("(");
            for(Parameter parameter : method.getParameters()){
                System.out.print(" " + parameter.getType().getSimpleName() + " " + parameter.getName());
            }
            System.out.println(")");

            // выведем все аннотации метода
            for(Annotation annotation : method.getAnnotations()){
                System.out.println("Все аннотации метода: " + annotation.toString());
            }

            // проверим наличие нашей аннотации и ее значения
            if(method.isAnnotationPresent(Secured.class)){
                System.out.println(" Аннотация Secured присутствует у метода" );
                System.out.println(" Значения присутствующей аннотации: " + method.getAnnotation(Secured.class).number() + " " + method.getAnnotation(Secured.class).string()) ; // можно сделать annotation.ToString
            }
            else{
                System.out.println(" Для этого метода аннотации нет");
            }


            System.out.println("\n");
        }


    }
}
